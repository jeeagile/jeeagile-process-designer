import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import 'bpmn-js/dist/assets/diagram-js.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-codes.css'
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css'

import 'bpmn-js-token-simulation/assets/css/bpmn-js-token-simulation.css'
import 'bpmn-js-token-simulation/assets/css/normalize.css'

import 'diagram-js-minimap/assets/diagram-js-minimap.css'

import 'bpmn-js-bpmnlint/dist/assets/css/bpmn-js-bpmnlint.css'

import 'element-ui/lib/theme-chalk/index.css'

import './assets/styles/element-variables.scss'
import './assets/styles/process-designer.scss'
import './assets/styles/process-properties.scss'

import ElementUI from 'element-ui'

Vue.use(ElementUI)

import { vuePlugin } from 'highlight.js'
import 'highlight.js/styles/atom-one-dark-reasonable.css'

Vue.use(vuePlugin)

new Vue({
  render: (h) => h(App)
}).$mount('#app')
